\section{Evaluation}
\label{sec:evaluation}


We evaluated and compared all four scenarios, implementing the required smart contracts in Solidity (the mainstream Ethereum language) and deploying them on Ethereum~\cite{ethereum_white}.
More specifically, we spawned our own private Ethereum instances for Scenarios~2 and~3, while we used the Ethereum Ropsten testbed as a public ledger.


We took a number of configuration decisions to reduce the parameter space and to allow for a fair comparison.
With respect to parameters of our local Ethereum instances, we fixed the average block mining time to 15 seconds, and we set the block gas limit to $10,000,000$ gas units. Both these values reflect the respective values in the public Ethereum main net. 

With respect to our implementation, we fixed the data schema of transactions across all four scenarios, as well as the lengths of several fields.
We also fixed both the anchoring period and the sensor logging period to 5 minutes, for all stages in all scenarios.

\begin{table}[b]
  \caption{Transaction Types}
  \label{tab:transactions}
  \begin{tabular}{|m{.19\linewidth}|m{.7\linewidth}|}
    \hline
    \textsc{Transaction}    & \textsc{Description} \\ \hline
    \textbf{Box entry}      & Marks the start of a new session for a given box, and assigns this box to a given farmer. \\ \hline
    \textbf{Handover}       & Hands over a box from one stage to the next, recording the two employee IDs involved, the box weight, and a timestamp. \\ \hline
    \textbf{Box exit}       & Marks the end of the current session of a given box. Called when the box is emptied and ready for a new session. \\ \hline
    \textbf{Sensor logging} & Logs a stage's sensor readings to the ledger. Applied periodically, every 5 minutes. \\ \hline
    \textbf{Anchoring}      & Stores a private Ethereum's latest block hash into the public Ethereum. Called periodically, every 5 minutes. \\ \hline
  \end{tabular}
  \vspace{\figlifting}
\end{table}

Table~\ref{tab:transactions} presents the five transaction types that form the basis for the FSC functionality supported in all considered scenarios.


Finally, Table~\ref{tab:fields} shows, for each of the basic transactions, the fields it stores in the ledger. Note that we assumed uniform format across all four handovers, and uniform length across all five stages' sensor logging transactions.


\begin{table}[h]
  \caption{Transaction Fields}
  \label{tab:fields}
  \centering
  \includegraphics[width=1\linewidth]{figs/lengths.pdf}
  \vspace{\figlifting}
\end{table}




\subsection{Cost Evaluation}

We start our evaluation focusing on the cost associated with each scenario for executing smart contracts.

In Ethereum, each call to a smart contract function incurs a cost to the caller. This cost is measured in \emph{gas units}, an Ethereum-specific metric deterministically derived based on the amount of CPU cycles, network traffic, and ledger storage caused by the call. Gas has no fixed monetary value. Instead, when calling a smart contract function you are expected to specify the rate (in ETH, i.e., the Ethereum coin) you are willing to pay per gas unit to whoever mines a block that includes your transaction. This policy serves as an incentive for miners to include your transaction.

\begin{figure}[b]
  \centering
  \includegraphics[width=\graphscale\linewidth]{data/gas_basic.pdf}
  \vspace{\figlifting}
  \caption{Ledger cost (in gas) for single execution of basic operations.}
  \label{fig:gas_basic}
  \vspace{\figlifting}
\end{figure}

%First we measure the cost of individual calls to the five basic operations, for each scenario implementation. \figref{fig:gas_basic} presents these costs in gas units. Box exit, sensor logging, and anchoring are uniform across all scenarios. However, box entry and handovers are slightly more costly when multiple ledgers are used (Sc.~3), as the box ID should be stored anew with each handover.

In \figref{fig:gas_basic} we present the gas cost of an individual call to each basic operation. Implementations are similar across different scenarios. However, box entry and handovers are slightly more costly when multiple ledgers are used (Scenario~3), as the box ID has to be stored anew with each handover.
Note that anchoring is not applicable for Scenario~1, as it already stores everything in the public ledger, while transactions (except for anchoring) are not applicable to Scenario~4, as it employs local storages.
Non-applicable entries are marked N/A.


\begin{figure}[t]
  \centering
  \includegraphics[width=\graphscale\linewidth]{data/gas_route.pdf}
  \vspace{\figlifting}
  \caption{Aggregate cost (in gas) for a full session of a single box through the FSC. Color coding denotes which ledger each cost fraction refers to.}
  \label{fig:gas_route}
  \vspace{\figlifting}
\end{figure}


\figref{fig:gas_route} shows the aggregate gas spent for an entire route of a single box through the FSC. Only the costs directly associated with tracing a box are included (i.e., box entry/exit and four handovers), leaving the costs of periodic anchoring and sensor logging out. As expected, the first two scenarios exhibit precisely the same costs, in gas. The third scenario incurs a slightly higher overall gas cost, however this cost is distributed among the four pair ledgers.
Finally, ledger operations are not applicable to Scenario~4, as explained above.

It is important to realize that only the gas spent in Scenario~1 translates into actual monetary value (in ETH, and indirectly in EUR). Gas spent on privately managed Ethereum instances can be paid for with ethers collected through very lightweight mining or an ether faucet.
%However, as we will see in \secref{sec:throughput}, the amount of gas spent by each operation has an effect on the total throughput of the system, as each mined block has an upper threshold on the amount of gas it accepts.
However, as we will see in \secref{sec:throughput}, the gas required by each operation has an effect on the overall throughput of the system in \emph{all} scenarios, as each mined block can include a limited amount of gas.

 


\figref{fig:periodic} displays the public ledger gas costs associated with each periodic action (anchoring and sensor logging). Note that Scenario~1 incurs periodic costs only for sensor logging, while anchoring is irrelevant to it as data is recorded directly in the public ledger. In contrast, Scenarios~2, 3, and 4 incur only periodic anchoring costs to the public ledger, as all sensor logging takes places in private ledgers or private storage.




Table~\ref{tab:costs} summarizes the costs presented above, giving an estimate of the total cost for a full day's (24 hours) operation in each scenario. We assume a typical daily turnover of 6000 boxes. For the conversion of gas to ether, we assume a price of $10^{-8}$ ETH per gas unit (referred to as 10 nanoether or 10 gwei). Finally, for the conversion to euros, we assume the price of \euro200 per Ether, an average price for the last few months. \figref{fig:fullday} illustrates these costs as a function of the number of boxes processed through the supply chain.

\begin{figure}[t]
  \centering
  \includegraphics[width=\graphscale\linewidth]{data/periodic.pdf}
  \vspace{\figlifting}
  \caption{Public ledger gas costs incurred every 5 minutes due to periodic operations. For Scenario~1 this accounts to the five sites directly recording sensor readings on the public ledger. For the other scenarios it accounts to periodic storing of block hashes on the public ledger (anchoring).}
  \label{fig:periodic}
  \vspace{\figlifting}
\end{figure}




\begin{table}[h]
  \caption{Full day operation cost}
  \label{tab:costs}
  \scriptsize
  %\begin{tabular}{|m{.19\linewidth}|m{.7\linewidth}|}
  \begin{tabular}{|c|c|c|c|c|c|c|c|}
    \hline
                & \multicolumn{3}{c|}{Cost for 6000 boxes} & \multicolumn{3}{c|}{Full-day periodic costs} & \textbf{Total} \\ \hline
                & Gas & Ether & EUR & Gas & Ether & EUR & \textbf{EUR} \\ \hline
    Scenario 1  & 2040M & 20.4 & \euro4080 & 71.5M & 0.715 & \euro143 & \textbf{\euro4223} \\ \hline
    Scenario 2  & 0     & 0    & \euro0    & 14.3M & 0.143 & \euro28  & \textbf{\euro28}   \\ \hline
    Scenario 3  & 0     & 0    & \euro0    & 57.2M & 0.572 & \euro114 & \textbf{\euro114}  \\ \hline
    Scenario 4  & 0     & 0    & \euro0    & 71.5M & 0.715 & \euro143 & \textbf{\euro143}  \\ \hline
  \end{tabular}
  \vspace{\figlifting}
\end{table}



\begin{figure}[b]
  \centering
  \includegraphics[width=\graphscale\linewidth]{data/fullday.pdf}
  \vspace{\figlifting}
  \caption{Total cost (in EUR) for a full day run in each scenario, as a function of the number of boxes transferred. Calculated based on an exchange rate of 200 EUR per 1 ETH, and a gas price of $10^{-8}$ ETH per gas unit.}
  \label{fig:fullday}
\end{figure}




%\subsection{Time Evaluation}


\subsection{Throughput Evaluation}
\label{sec:throughput}

Cost is not the only concern when employing blockchains in a real-world application.
The time to execute a transaction, and in particular the volume of transactions the system can process in a given time window (i.e., the system's \emph{throughput}), may be a deciding factor on designing its architecture.

The limitations on transaction delay and throughput are explicitly imposed by blockchain rules that prevent the creation of arbitrary-size blocks. In the case of Ethereum, this limit is imposed by means of a maximum amount of gas a miner is willing to pack in a single block. Although this is a miner-specific parameter rather than a globally configured one, in main net public Ethereum it has converged to a maximum of 10 million gas units per block.
In that regard, optimizing smart contract functions to spend less gas allows us to fit more calls per block, thus increasing the overall throughput.

To assess the throughput of different scenarios, we ran experiments in which we submitted 1000 boxes at once at the beginning of the FSC, and we let the system run as fast as possible to record the rate at which boxes go through the chain.
In these experiments, we emulated all harvesting, transportation, storage, and selling times to be zero, in order to assess the net delays imposed by our tracing system.
\figref{fig:throughput} presents results of these experiments, namely the number of boxes that have traversed the entire FSC as a function of time. The
%round dots correspond to new block generations and they occur
stepwise shape of these plots is due to the grouping of multiple transactions in blocks, which are being generated
roughly every 15 seconds. As each box needs six transactions to traverse the entire FSC, no box makes it to the end before the sixth block (at $\sim$90 seconds).

Scenario~3 has a clear advantage over the rest, with a throughput of around 285 boxes per minute, while Scenarios~1 and~2 perform at around 111 and 133 boxes per minute, respectively.

%The fact that Scenario~3's throughput is more than double that of Scenario~2 was expected, considering that the latter imposes to its shared ledger more than double the maximum gas cost the former imposes on any of its pair ledgers (\figref{fig:gas_route}). Scenario~3's overall gas cost has no effect on its throughput, as this cost is distributed across four distinct ledgers.

Although intuitively one might expect Scenario~3's throughput to be four times as high as that of Scenario~2, given the fact that transactions are distributed across four private ledgers instead of just one, it is in fact just over twice as high. The reason for this is that in queuing systems throughput is governed by the speed of the slowest component in a chain, i.e., its bottleneck. In this case, Scenario~3's bottleneck is the leftmost pair ledger (shared between stages~1 and~2), as it registers both box-entry transactions for registering new boxes, as well as handovers from stage~1 to stage~2. As shown in \figref{fig:gas_route}, the total gas used on the single shared ledger of Scenario~2 (which is what determines how many transactions fit in one block) is a bit over twice as much as the total gas spent on the leftmost pair ledger of Scenario~3, a ratio reflected on their respective throughputs.

Finally, Scenario~1 seems to perform slightly worse than Scenario~2. This is because it ran on a public Ethereum instance, Ropsten, where our transactions were not guaranteed to be included in the next block as they were competing against other users' transactions.




\begin{figure}[t]
  \centering
  \includegraphics[width=\graphscale\linewidth]{data/throughput.pdf}
  %\includegraphics{data/throughput.pdf}
  \vspace{\figlifting}
  \caption{Box handling throughput.}
  \label{fig:throughput}
  \vspace{\figlifting}
\end{figure}

