\section{Architecture Space}
\label{sec:architecture}


The goal of the proposed FSC framework is to provide produce traceability in a multi-party business environment of independent entities with potentially conflicting interests. Thus, transparency, data integrity, and building of trust between chain members are key elements in this system. We satisfy these relying on distributed ledger technology.

We consider four different architectures for the proposed FSC framework.
Two of them use a single public ledger, while the other two employ hierarchical designs involving a combination of public and private ledgers.





\begin{figure*}[t]
  \begin{minipage}[b][][b]{0.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{figs/scenario3.pdf}
    \caption{Scenario 3 -- One private ledger per pair}
    %\caption{\textbf{Scenario 3 -- One private ledger per pair:} Each pair of consecutive stages maintain a separate ledger for recording box handovers between themselves. Overall scalability and throughput are increased. Anchoring to a public ledger is necessary to guarantee immutability.}
    \label{fig:scenario3}
  \end{minipage}
  \hfill
  \begin{minipage}[b][][b]{0.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{figs/scenario4.pdf}
    \caption{Scenario 4 -- Private storage}
    %\caption{\textbf{Scenario 4 -- Private storage:} Each stage maintains their own private storage. This increases scalability, privacy with respect to sensitive data, and ease of management. Anchoring of data hashes to a public ledger is still necessary, though, to guarantee immutability.}
    \label{fig:scenario4}
  \end{minipage}
  \vspace{-6pt}
\end{figure*}



More specifically, we consider the following four architecture scenarios:

\begin{description}

  \item[Scenario 1 -- Public ledger:] In this scenario, both handover data as well as sensor readings, are directly stored in a public ledger. This is a straightforward architecture, inherently guaranteeing immutability and trust among chain members. However, as we will see in our evaluation (\secref{sec:evaluation}), the large volume of data to be stored place an enormous burden in terms of cost and increase delay.

  \item[Scenario 2 -- Single shared ledger:] Architecturally this scenario is identical to the previous one, other than using a shared private ledger (run collaboratively by all chain members) in place of a public ledger, to avoid high costs and delays. A public ledger, however, is still needed to add strong immutability guarantees to the private ledger. More specifically, the private ledger's latest block hash is periodically stored on the public ledger to strengthen the former's immutability, a process referred to as \emph{anchoring}.

  \item[Scenario 3 -- One private ledger per pair:] This scenario employs multiple private ledgers, one per pair of adjacent chain stages. It improves on Scenario~2 with respect to data privacy, as well as overall throughput (\secref{sec:throughput}). Anchoring to a public ledger is necessary here too, to guarantee immutability.

  \item[Scenario 4 -- Private storage:] This scenario maximizes privacy with respect to sensitive data, by each business entity storing all their data in private storages. These storages need not be ledgers (although they could be). They can be local databases, cloud storages, or even local permissioned ledgers. In the absence of a ledger to store mutually approved handover transactions between adjacent stages, handover records should be \emph{signed} by both stages involved, and stored independently by both of them. In order to guarantee immutability of private storages, each stage is responsible to implement anchoring for their private storage by periodically storing a hash digest (Merkle tree root or alternative cryptographic tool of their choice) of their contents. 

\end{description}

Clearly, an unlimited number of architecture designs can be devised, either as combinations of the above or by introducing completely new schemes. However, the  selected scenarios form a wide range of clearly defined baselines, whose evaluation identifies their strengths and weaknesses, and help us make educated decisions in using them as is or in combinations.
