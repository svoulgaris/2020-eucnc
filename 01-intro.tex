\vspace{\figlifting}

\section{Introduction}

The Food Supply Chain (FSC) sector is currently under intensive transformation to meet the challenges of the supply chain 4.0 vision \cite{Tjahjono2017-industry4.0}. The application of emerging technologies, such as the Internet of Things (IoT), big data analytics, autonomous robotics, etc., drive its digitization and adds value to the supply chain management through increased automation, transparency, and product traceability, improved product quality and safety control, and better-quality relationships between suppliers and buyers \cite{Thoben2017}, \cite{Buyukozkan2018}, \cite{Ben-Daya2019}. Nevertheless, the majority of the proposed solutions still rely on heavily-centralized infrastructures and central control authorities to manage data and offer end-to-end services. As a result, this limits transparency and, by nature, creates major concerns related to data security and integrity, protection of enterprise (sensitive) data, and building of mutual trust between chain members. In this scope, blockchains represent an innovative technological solution not only to support automation in data and transactions management but also to build trust amongst trustless entities \cite{Francisco2017}, \cite{Kshetri2018}.

%establish a trustless environment for all participants of the chain on top of their inherent properties which provide fault-tolerance, data immutability, transparency and secure access in common resources \cite{Francisco2017}, \cite{Kshetri2018}.

%In recent years, various research studies identified food supply chains as essential areas for deploying blockchain technology and combine it with other disruptive ICT. In \cite{Perboli2018-supplyChain}, the authors study the suitability of blockchains for logistics and supply chain and devise a methodology to link the technology design process with the business process modeling based on the needs and the objectives of the different actors involved. Their analysis and proposed business models highlight significant returns of investments in reducing the logistics costs and optimizing the efficiency of supply chain management operations. In \cite{Kamilaris2019}, the most promising recent blockchain technology initiatives and projects in the agriculture and food supply sectors are presented, and their overall implications, barriers, challenges and potentials are thoroughly discussed in both technological and economic aspects. As concluded, one of the most critical factors for the success of blockchain-minded ecosystems in the agrifood chains is the definition of a concrete regulation framework which will boost both their technological maturity and economic sustainability.


\begin{figure}[t]
  \centering
  \includegraphics[width=1\linewidth]{figs/model.pdf}
  \vspace{\figlifting}
  \vspace{\figlifting}
  \caption{Food Supply Chain with five stages.}
  \label{fig:model}
  \vspace{\figlifting}
  \vspace{-8pt}
\end{figure}

Traceability in FSC has gained considerable importance during the last years, particularly as a means to assure food safety and quality, minimize the production and distribution of poor quality products, and increase consumers' confidence in labels and brands \cite{vandorp2002-tracking}, \cite{kelepouris2007-rfid}, \cite{Aung2014}. The implementation of system prototypes that apply blockchain technology in specific FSC scenarios to offer traceability and food safety is an active field. In \cite{Wang2019}, a product traceability system is developed based on blockchain technology, smart contracts, and IoT, orchestrating product transferring and tracking through the collaboration of a number of smart contracts ensuring that data is transparent and tamper-proof, providing different levels of data query functions for different actors. A similar design is proposed in \cite{Lin2019} where blockchain is combined with EPC information service network to reduce data redundancy and enforce corporate privacy and data protection at an enterprise level. 

%A distributed, trustless, and secure model that uses a private blockchain and smart contracts to offer traceability and food quality services over the FSC is also proposed in \cite{Casino2019-traceability}. The proposed model also introduces a group of Key Performance Indicators (KPIs), such as efficiency, responsiveness, required trust, resiliency etc., which can be used to compare against traditional FSC traceability mechanisms. More blockchain and IoT traceability prototypes have also been presented in~\cite{Tian2017} and~\cite{Caro2018-agrifood}. In~\cite{Tian2017}, blockchain is combined with RFID technology and Hazard Analysis and Critical Control Points to reduce risks of unsafe practices and hazards in food processing, while a fully decentralized, blockchain-based traceability solution is proposed in \cite{Caro2018-agrifood} to enable the seamless integration of different types of IoT devices and data sources. Interestingly, the performance of the prototype is evaluated by using two different blockchain implementations, namely Ethereum and Hyperledger Sawtooth, and the main advantages and disadvantages of each approach were analyzed.

Despite that many prototypes exist that explore the capabilities of blockchain technology in securing product quality and optimizing supply chain management, most of these refer to scenarios where a single blockchain network is deployed to enable transactions and streamline information over the supply chain. An analysis of how more complex and hybrid topologies of blockchain networks can orchestrate the FSC management and affect performance criteria related to the cost, the speed and the throughput of transactions remains to be determined.
Combining different types of blockchains, and in particular public and permissioned blockchains, allows different tradeoffs in terms of trust decentralization, transparency, privacy, transaction cost, and delay.

Motivated by this, in this paper we propose a fully decentralized, blockchain-based, end-to-end traceability and data logging solution for FSCs and we evaluate its performance for four alternative hierarchical topologies of cooperating ledgers that record data and achieve immutability and transparency. To the best of our knowledge, such an evaluation has not appeared in the literature before.

%The remainder of the paper is organized as follows: In \secref{sec:model}, the FSC model we consider is introduced and its stages are described. In \secref{sec:architecture}, the proposed system architecture offering end-to-end traceability of products is presented and the four scenarios employing blockchain technology to orchestrate data management are described. \secref{sec:evaluation} evaluates the four scenarios by presenting and discussing results. Finally, we conclude in \secref{sec:conclusion}.


\begin{figure*}[t]
  \centering
  \begin{minipage}[b][][b]{0.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{figs/scenario1.pdf}
    \caption{Scenario 1 -- Public ledger}
    %\caption{\textbf{Scenario 1 -- Public ledger:} All sensor data (dashed green lines) and handover data (solid blue lines) are registered in a public ledger. Immutability is guaranteed, alas at a high cost.}
    \label{fig:scenario1}
  \end{minipage}
  \hfill
  \begin{minipage}[b][][b]{0.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{figs/scenario2.pdf}
    \caption{Scenario 2 -- Single shared ledger}
    %\caption{\textbf{Scenario 2 -- Single shared ledger:} All sensor and handover data are registered in a shared ledger operated by the entire consortium. The cost is kept low, however in order to guarantee immutability block hashes of the shared ledger are regularly registered in a public ledger.}
    \label{fig:scenario2}
  \end{minipage}
  \vspace{\figlifting}
\end{figure*}

